#! /bin/sh

# postfix and dovecot setup script (instead of sendmail)
# based on http://centossrv.com/postfix.shtml

echo "### install postfix via yum if not exist"
if [ ! -e /etc/postfix/main.cf ]
then
    sudo yum -y install postfix
else
    echo "postfix already installed"
fi

echo "### edit conf file"
sudo cp /etc/postfix/main.cf /etc/postfix/main.cf.orig.`date %Y%m%d`
sudo vi /etc/postfix/main.cf

echo "### start saslauthd"
sudo /etc/rc.d/init.d/saslauthd status
if [ $? -ne 0 ]
then
    sudo /etc/rc.d/init.d/saslauthd restart
    if [ $? -ne 0 ]
    then
        sudo /etc/rc.d/init.d/saslauthd start
    fi
fi
sudo /sbin/chkconfig saslauthd on

echo "### stop sendmail"
sudo /etc/rc.d/init.d/sendmail status
if [ $? -ne 3 ]
then
    sudo /etc/rc.d/init.d/sendmail stop
    if [ $? -ne 0 ]
    then
        echo "### FAIL: cannot stop sendmail"
        exit 1
    fi
fi
sudo /sbin/chkconfig sendmail off

echo "### switch mta (you need to choice postfix)"
sudo /usr/sbin/alternatives --config mta

echo "### start postfix"
sudo /etc/rc.d/init.d/postfix status
if [ $? -ne 0 ]
then
    sudo /etc/rc.d/init.d/postfix start
    if [ $? -ne 0 ]
    then
        echo "FAIL: cannot start postfix"
        exit 1
    fi
fi
sudo /sbin/chkconfig postfix on

echo "### install dovecot"
if [ ! -e /etc/dovecot.conf ]
then
    sudo yum -y install dovecot
else
    echo "dovecot already installed"
fi

echo "### edit conf file"
sudo cp /etc/dovecot.conf /etc/dovecot.conf.orig.`date %Y%m%d`
sudo vi /etc/dovecot.conf

echo "### start dovecot"
sudo /etc/rc.d/init.d/dovecot status
if [ $? -ne 0 ]
then
    sudo /etc/rc.d/init.d/dovecot start
    if [ $? -ne 0 ]
    then
        echo "FAIL: cannot start dovecot"
        exit 1
    fi
fi
sudo /sbin/chkconfig dovecot on

echo "### done"
